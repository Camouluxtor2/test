import java.io.BufferedReader;
import java.io.IOException;

class Liste {
	
	CoupleLettreMorse cp;
	Liste suiv;
	
	Liste(CoupleLettreMorse r, Liste li) {
		cp = r;
		suiv = li;
	}
	
	static boolean estVide(Liste l) {
		return l==null;
	}
	
	static CoupleLettreMorse getCouple(Liste l) {
		return l.cp;
	}
	
	static char lettre(Liste l) {
		return CoupleLettreMorse.getLettre(getCouple(l));
	}
	
	static String morse(Liste l) {
		return CoupleLettreMorse.getMorse(getCouple(l));
	}
	
	static String lettreToMorse(char c, Liste l) {
		if(c == ' ')
			return " ";
		if(!estVide(l)) {
			if(lettreAppartient(c,l)) {
				while(lettre(l) != c) {
					l = reste(l);
				}
				return morse(l);
			}
		}
		return null;
	}
	
	static char morseToLettre(String s, Liste l) {
		if(!estVide(l)) {
			if(morseAppartient(s,l)) {
				while(!morse(l).equals(s)) {
					l = reste(l);
				}
				return lettre(l);
			}
		}
		return '�';
	}
	
	static Liste reste(Liste l) {
		return  l.suiv;
	}
	
	static Liste constr(CoupleLettreMorse cp, Liste p) {
		return new Liste(cp, p);
	}
	
	static void afficher(Liste p) {
		while(!estVide(p)) {
			System.out.println(lettre(p)+"                "+morse(p));
			p = reste(p);
		}
	}
	
	static boolean lettreAppartient(char x, Liste l) {
		if(x == lettre(l))
			return true;
		else {
			if(reste(l) != null)
				return lettreAppartient(x,reste(l));
			else
				return false;
		}
	}
	
	static boolean morseAppartient(String x, Liste l) {
		if(x.equals(morse(l))) {
			return true;
		}
		else {
			if(reste(l) != null)
				return morseAppartient(x,reste(l));
			else
				return false;
		}
	}
	
	static int longueur(Liste p) {
		if(estVide(p))
			return 0;
		else
			return 1 + longueur(reste(p));
	}
	
	static Liste ajouter(CoupleLettreMorse cp, Liste l) {
		if(estVide(l)) {
			l = constr(cp,null);
		}else {
			while(reste(l) != null) {
				l = reste(l);
			}
		l.suiv = constr(cp,null);
		}
		return l;
	}
	
	
	static String traduireLettreToMorse(String s,Liste l) {
		String s2 = "";
		for(int i=0;i<s.length();i++) {
			s2 += lettreToMorse(s.charAt(i),l);
			s2 += '/';
		}	
		return s2;
	}
	
	static String traduireMorseToLettre(String s,Liste l) {	
		String s2 = "";
		int i = 0;
		int j = 0;
		boolean b;
		while(i<s.length()) {
			j = i;
			b = false;
			while(i<s.length() && !b) {
				if(s.charAt(i) != '/') {
					i++;
				}else {
					b = true;
				}
			}
			if(b) {
				s2 += morseToLettre(s.substring(j, i),l);
			}
			i++;
		}
		return s2;
	}
	
	static Liste creerListe(BufferedReader s) throws IOException {
		
		Liste l = null;
		String strCurrentLine;
		String tmp[] = new String[2];
		if((strCurrentLine = s.readLine()) != null) {
			tmp = strCurrentLine.split(" ");
			l = Liste.ajouter(new CoupleLettreMorse(tmp[0].charAt(0),tmp[1]), l);
		}
	    while ((strCurrentLine = s.readLine()) != null) {
	    	tmp = strCurrentLine.split(" ");
	    	Liste.ajouter(new CoupleLettreMorse(tmp[0].charAt(0),tmp[1]), l);
	    }
		return l;
		
	}
	

}
