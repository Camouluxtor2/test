import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Fenetre extends JFrame implements ActionListener{
	
	private JButton jb,jb2,jb3,jb4;
	private JTextArea jt,jt2;
	private Liste l;
	private Arbre a;
	
	public Fenetre(Arbre ar,Liste li){
	    this.setTitle("Traducteur");
	    this.setSize(970, 700);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.setLocationRelativeTo(null);
	    
	    this.a = ar;
	    this.l = li;
	    
	    JPanel panel = new JPanel();
	    panel.setLayout(null);
	    this.add(panel);
	   
	    this.jb = new JButton("import fichier texte");
	    this.jb.addActionListener(this);
	    this.jb2 = new JButton("import fichier morse");
	    this.jb2.addActionListener(this);
	    this.jb3 = new JButton("export dans un fichier text");
	    this.jb3.addActionListener(this);
	    this.jb4 = new JButton("export dans un fichier text");
	    this.jb4.addActionListener(this);
	    this.jt = new JTextArea();
	    this.jt2 = new JTextArea();
	    
	    panel.add(jb);
	    panel.add(jb2);
	    panel.add(jb3);
	    panel.add(jb4);
	    panel.add(jt);
	    panel.add(jt2);
	    jb.setBounds(50, 40, 400, 60);
	    jb2.setBounds(500, 40, 400, 60);
	    jb3.setBounds(50, 570, 400, 60);
	    jb4.setBounds(500, 570, 400, 60);
	    jt.setBounds(50, 130, 400, 400);
	    jt2.setBounds(500, 130, 400, 400);
	    
	    this.setVisible(true);
	}

	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		if(source == jb){
			try {	this.importTexte();	} catch (Exception e) {}
		} else if(source == jb2){
			try {	this.importMorse();	} catch (Exception e) {}	
		} else if(source == jb3) {
			try {	this.exporterMorse();	} catch (Exception e) {}
		}
	}      
	
	public void importTexte() throws IOException {
		JFileChooser fc = new JFileChooser();
		
		int returnVal = fc.showOpenDialog(Fenetre.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            BufferedReader reader = new BufferedReader(new FileReader(file.getAbsolutePath()));
            String s;
            while((s = reader.readLine()) != null) {
            	this.jt.insert(Liste.traduireLettreToMorse(s, this.l),0);
            }
        }
	}
	
	public void importMorse() throws IOException {
		JFileChooser fc = new JFileChooser();
		
		int returnVal = fc.showOpenDialog(Fenetre.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            BufferedReader reader = new BufferedReader(new FileReader(file.getAbsolutePath()));
            String s;
            while((s = reader.readLine()) != null) {
            	this.jt2.insert(Arbre.traduireLong(s, this.a),0);
            }
        }
	}
	
	public void exporterLettre() throws IOException {
		FileOutputStream sortie = new FileOutputStream("H:/Export.txt");
		BufferedWriter texte = new BufferedWriter(new FileWriter("H:/Export.txt"));
		
		texte.write(this.jt.getText());
		
		texte.close();
	}
	
	public void exporterMorse() throws IOException {
		FileOutputStream sortie = new FileOutputStream("H:/Export.txt");
		
		BufferedWriter texte = new BufferedWriter(new FileWriter("H:/Export.txt"));
		
		texte.write(this.jt2.getText());
		
		texte.close();
	}
	
}


