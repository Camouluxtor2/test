
public class CoupleLettreMorse {
	
	char lettre;
	String morse;
	
	CoupleLettreMorse(char c, String s) {
		lettre = c;
		morse = s;
	}
	
	static String getMorse(CoupleLettreMorse cp) {
		return cp.morse;
	}
	
	static char getLettre(CoupleLettreMorse cp) {
		return cp.lettre;
	}
	
	static void sys(CoupleLettreMorse cp) {
		System.out.println(cp.lettre);
		System.out.println(cp.morse);
	}

}
