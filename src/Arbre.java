import java.io.BufferedReader;
import java.io.IOException;

public class Arbre {

	Arbre gauche;
    Arbre droite;
    char donnee;

    static Arbre Arbre(char data)
    {
    	Arbre a = new Arbre();
    	a.donnee = data;
    	return a;
    }
    
    static void ajouter(Arbre gauch, Arbre droit, Arbre a)
    {
    	if(a == null) { a = new Arbre(); }
    	if (gauch != null)
    	{
    		if(a.gauche == null)
    			a.gauche = gauch;
    	}
    	if (droit != null)
    	{
    		if(a.droite == null)
    			a.droite = droit;
    	}
    }
    
    static void ajouterMorse(String lettre, String s, Arbre a) {
    	Arbre b = a;
    	int i;
    	for(i=0;i<s.length();i++) {
    		if(s.charAt(i) == '.') {
    			Arbre.ajouter(null, new Arbre(), b);
    			b = Arbre.avancer("droite", b);
    		}
    		else {
    			Arbre.ajouter(new Arbre(), null, b);
    			b = Arbre.avancer("gauche", b);
    		}
    	}
    	
    	i--;
    	
    	b.donnee = lettre.charAt(0);
    	
    }
    
    static Arbre avancer(String orientation, Arbre a)
    {
    	if (orientation.equals("gauche"))
    	{
    		a = a.gauche;
    	}
    	else
    	{
    		a = a.droite;
    	}
		return a;
    }
    
    static char traduireChar(String donnee, Arbre a)
    {
    	int i = 0;
    	Arbre b = a;
    	while (i < donnee.length())
    	{
    		if (donnee.charAt(i) == '.')
    		{
    			b = avancer("droite", b);
    		}
    		else
    		{
    			b = avancer("gauche", b);
    		}
    		i++;
    	}
    	return b.donnee;
    }
    
    static String traduireLong(String donnee, Arbre a)
    {
    	String s2 = "";
    	int i = 0;
    	int j = 0;
    	boolean b;
    	while (i<donnee.length())
    		{
    		System.out.println(i);
    			if(donnee.charAt(i) == ' ') { s2 += " "; i++; }
    			else {
	    			j=i;
	    			b = false;
	    			while (i<donnee.length() && !b)
	    			{
	    				if (donnee.charAt(i) != '/')
	    				{
	    					i++;
	    				}else
	    				{
	    					b = true;
	    				}
	    			}
	    			if(b)
	    			{
	    				s2 += traduireChar(donnee.substring(j,i),a);
	    			}
	    			
	    			i++;
	    		}
    		}
    	return s2;
    }
    
	static Arbre creerArbre(BufferedReader s) throws IOException {
		Arbre a = new Arbre();
		String strCurrentLine;
		String tmp[] = new String[2];
	    while ((strCurrentLine = s.readLine()) != null) {
	    	tmp = strCurrentLine.split(" ");
	    	ajouterMorse(tmp[0], tmp[1], a);
	    }
		return a;
	}
	
}
